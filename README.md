# money-transfer-api
RESTful API for money transfers between accounts.
##Design Decisions
- Transfers can only be made between accounts having the same currency code
- Overdrafts are not allowed i.e. 0 is the minimum balance

## Technologies
- JAX-RS
- H2Datastore
- Java
- Unirest
- Jetty Container

## How to run
- mvn exec:java runs application on localhost port: 8081
- Tests run on localhost port:7070

## Endpoints
- Account
    1. Create Account: /account/createAccount/{accountHolderName}/{currencyCode}/{balance} TYPE: PUT
    2. Get Account: /account/{accountID} TYPE: GET
    3. Delete Account: /account/{accountID} TYPE: DELETE
    4. Deposit: /account/deposit/{accountID}/{amount} TYPE: PUT
    5. Withdraw: /account/withdraw/{accountID}/{amount} TYPE: PUT
    6. Get all accounts: /account/accounts TYPE: GET
    
- AccountHolder
    1. Create AccountHolder: /accountHolder/createAccountHolder/{accountHolderName}/{emailAddress}/{personalID} TYPE: PUT
    2. Get AccountHolder: /accountHolder/{personalID} TYPE: GET
    3. Delete AccountHolder: /accountHolder/{accountHolderID} TYPE: DELETE
    4. Update AccountHolder: /accountHolder/{accountHolderID} TYPE: PUT
    4. Get all AccountHolders: /account/accountHolders TYPE: GET
    
- Transfer
    1. /transfer/{accountFromID}/{accountToID}/{amount} TYPE: POST