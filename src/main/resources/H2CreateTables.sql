DROP TABLE IF EXISTS Account;
DROP TABLE IF EXISTS AccountHolder;

CREATE TABLE Account (
    AccountID LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    AccountHolderName VARCHAR(50),
    Balance DECIMAL(18,2),
    CurrencyCode CHAR(3)
);

CREATE UNIQUE INDEX uidx_account on Account(AccountHolderName, CurrencyCode, Balance);

CREATE TABLE AccountHolder (
    AccountHolderID LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    AccountHolderName VARCHAR(50) NOT NULL,
    EmailAddress VARCHAR(100) NOT NULL,
    PersonalID VARCHAR(30) NOT NULL
);

CREATE UNIQUE INDEX uidx_accountHolder on AccountHolder(PersonalID);