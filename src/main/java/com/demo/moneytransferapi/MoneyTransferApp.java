package com.demo.moneytransferapi;

import com.demo.moneytransferapi.controller.AccountController;
import com.demo.moneytransferapi.controller.AccountHolderController;
import com.demo.moneytransferapi.controller.TransferController;
import com.demo.moneytransferapi.utils.H2DatastoreFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class MoneyTransferApp {

    public static void main(String[] args) throws Exception {
        H2DatastoreFactory h2DatastoreFactory = new H2DatastoreFactory();
        H2DatastoreFactory.createTablesInH2();
        start();
    }

    private static void start() throws Exception{
        Server server = new Server(8081);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/*");
        server.setHandler(handler);
        ServletHolder servletHolder = handler.addServlet(ServletContainer.class, "/*");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AccountHolderController.class.getCanonicalName());
        stringBuilder.append(",");
        stringBuilder.append(AccountController.class.getCanonicalName());
        stringBuilder.append(",");
        stringBuilder.append(TransferController.class.getCanonicalName());
        servletHolder.setInitParameter("jersey.config.server.provider.classnames", stringBuilder.toString());

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }
}
