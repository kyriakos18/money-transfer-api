package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.dao.AccountImplementation;
import com.demo.moneytransferapi.dao.TransferImplementation;
import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.DefaultTransfer;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
public class TransferController {
    private final AccountImplementation accountImplementation = new AccountImplementation();
    private final TransferImplementation transactionImplementation = new TransferImplementation();
    private static final Logger LOGGER = Logger.getLogger(TransferController.class);

    @POST
    @Path("/{accountFromID}/{accountToID}/{amount}")
    public Response performTransfer (@PathParam("accountFromID") long accountFromID,
                                     @PathParam("accountToID") long accountToID,
                                     @PathParam("accountToID")BigDecimal amount) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request to transfer amount: %s from account with AccountID: %s to account " +
                "with AccountID", amount.setScale(2, RoundingMode.HALF_EVEN), accountFromID, accountToID));
        DefaultAccount accountFrom = accountImplementation.getAccount(accountFromID);
        DefaultAccount accountTo = accountImplementation.getAccount(accountToID);
        if (accountFrom.getCurrencyCode().equals(accountTo.getCurrencyCode())) {
            DefaultTransfer transaction = new DefaultTransfer(accountFromID, accountToID, amount, accountFrom.getCurrencyCode());
            Response.Status status = transactionImplementation.performTransfer(transaction) == 2 ? Response.Status.OK : Response.Status.NOT_MODIFIED;
            return Response.status(status).build();
        }
        LOGGER.debug(String.format("Currency mismatch between accounts. Account From CCY: %s Account To CCY: %s",
                accountFrom.getCurrencyCode(), accountTo.getCurrencyCode()));
        throw new WebApplicationException("Currency mismatch between accounts", Response.Status.BAD_REQUEST);
    }
}
