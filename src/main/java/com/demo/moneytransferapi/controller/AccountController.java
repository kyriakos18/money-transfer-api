package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.dao.AccountImplementation;
import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.log4j.Logger;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

    private final AccountImplementation accountImplementation = new AccountImplementation();
    private static final Logger LOGGER = Logger.getLogger(AccountController.class);
    @PUT
    @Path("/createAccount/{accountHolderName}/{currencyCode}/{balance}")
    public DefaultAccount createAccount(@PathParam("accountHolderName") String accountHolderName,
                                        @PathParam("currencyCode") String currencyCode,
                                        @PathParam("balance") BigDecimal balance) throws MoneyTransferAPIException {
        DefaultAccount account = new DefaultAccount(accountHolderName, balance.setScale(2, RoundingMode.HALF_EVEN), currencyCode);
        LOGGER.debug("Request for creating" + account);
        try {
            long accountID = accountImplementation.createAccount(account);
            LOGGER.debug(String.format("AccountID: %s created for %s", accountID, account));
            return accountImplementation.getAccount(accountID);
        } catch (MoneyTransferAPIException ex) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

    }

    @PUT
    @Path("/deposit/{accountID}/{amount}")
    public DefaultAccount deposit(@PathParam("accountID") long accountID, @PathParam("amount") BigDecimal amount) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for depositing %s in account with AccountID: %s",
                amount.setScale(2, RoundingMode.HALF_EVEN), accountID));
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            accountImplementation.depositTo(accountID, amount.setScale(2, RoundingMode.HALF_EVEN));
            return accountImplementation.getAccount(accountID);
        }
        LOGGER.debug(String.format("Amount %s provided is less than or equal to 0.",
                amount.setScale(2, RoundingMode.HALF_EVEN)));
        throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
    }

    @PUT
    @Path("/withdraw/{accountID}/{amount}")
    public DefaultAccount withdraw(@PathParam("accountID") long accountID, @PathParam("amount") BigDecimal amount) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for withdrawing amount %s for account with AccountID: %s",
                amount.setScale(2, RoundingMode.HALF_EVEN), accountID));

        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            try {
                accountImplementation.withdrawFrom(accountID, amount.setScale(2, RoundingMode.HALF_EVEN));
                return accountImplementation.getAccount(accountID);

            } catch (MoneyTransferAPIException ex) {
                throw new WebApplicationException("Failed to perform withdrawal ", Response.Status.BAD_REQUEST);
            }
        } else {
            LOGGER.debug(String.format("Amount %s provided is less than or equal to 0.",
                    amount.setScale(2, RoundingMode.HALF_EVEN)));
            throw new WebApplicationException("Invalid withdrawal amount ", Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Path("/{accountID}")
    public DefaultAccount getAccount(@PathParam("accountID") long accountID) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for fetching account with AccountID: %s", accountID));
        return accountImplementation.getAccount(accountID);
    }

    @DELETE
    @Path("/{accountID}")
    public Response deleteAccount(@PathParam("accountID") long accountID) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for deleting account with AccountID: %s", accountID));
        Status status = accountImplementation.deleteAccount(accountID) == 1 ? Response.Status.OK : Response.Status.NOT_MODIFIED;
        return Response.status(status).build();
    }

    @GET
    @Path("/accounts")
    public List<DefaultAccount> getAccounts() throws MoneyTransferAPIException {
        LOGGER.debug("Request for fetching all accounts");
        return accountImplementation.getAllAccounts();
    }
}
