package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.dao.AccountHolderImplementation;
import com.demo.moneytransferapi.model.DefaultAccountHolder;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/accountHolder")
@Produces(MediaType.APPLICATION_JSON)
public class AccountHolderController {

    private final AccountHolderImplementation accountHolderImplementation = new AccountHolderImplementation();
    private static final Logger LOGGER = Logger.getLogger(AccountHolderController.class);

    @POST
    @Path("/createAccountHolder/{accountHolderName}/{emailAddress}/{personalID}")
    public DefaultAccountHolder createAccountHolder(@PathParam("accountHolderName") String accountHolderName,
                                                    @PathParam("emailAddress") String emailAddress,
                                                    @PathParam("personalID") String personalID) throws MoneyTransferAPIException {
        LOGGER.debug("Request for creating account holder with Name: " + accountHolderName + " Email: " + emailAddress
                + " PersonalID: " + personalID);
        if (accountHolderImplementation.checkIfAccountHolderExists(personalID) == 0){
            DefaultAccountHolder accountHolder = new DefaultAccountHolder(accountHolderName, emailAddress, personalID);
            long accountHolderID = accountHolderImplementation.createAccountHolder(accountHolder);
            accountHolder.setAccountHolderID(accountHolderID);
            return accountHolder;
        }
        LOGGER.error(String.format("Account Holder with Name: %s Email: %s PersonalID: %s already exists",
                accountHolderName, emailAddress, personalID));
        throw new WebApplicationException(String.format("Account Holder with Name: %s Email: %s PersonalID %s",
                accountHolderName, emailAddress, personalID), Response.Status.BAD_REQUEST);
    }

    @DELETE
    @Path("/{accountHolderID}")
    public Response deleteAccountHolder(@PathParam("accountHolderID") long accountHolderID) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for deleting account holder with AccountHolderID: %s", accountHolderID));
        Response.Status status = accountHolderImplementation.deleteAccountHolder(accountHolderID) == 1 ? Response.Status.OK : Response.Status.NOT_MODIFIED;
        return Response.status(status).build();
    }

    @PUT
    @Path("/{accountHolderID}")
    public Response updateAccountHolder(@PathParam("accountHolderID") long accountHolderID, DefaultAccountHolder accountHolder) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for updating account holder with AccountHolderID: %s", accountHolderID));
        Response.Status status = accountHolderImplementation.updateAccountHolderInfo(accountHolder) == 1 ? Response.Status.OK : Response.Status.FOUND;
        return Response.status(status).build();
    }

    @GET
    @Path("/{personalID}")
    public DefaultAccountHolder getAccountHolder(@PathParam("personalID") String personalID) throws MoneyTransferAPIException {
        LOGGER.debug(String.format("Request for fetching account holder with PersonalID: %s", personalID));
        if (accountHolderImplementation.checkIfAccountHolderExists(personalID) > 0){
            return accountHolderImplementation.getAccountHolder(personalID);
        }
        throw new WebApplicationException(String.format("Could not find Account Holder with PersonalID: %s",
                personalID), Response.Status.BAD_REQUEST);
    }

    @GET
    @Path("/accountHolders")
    public List<DefaultAccountHolder> getAccountHolders() throws MoneyTransferAPIException {
        LOGGER.debug("Request for fetching all account holders");
        return accountHolderImplementation.getAllAccountHolders();
    }
}
