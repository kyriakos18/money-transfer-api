package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.model.exception.MoneyTransferAPIError;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MoneyTransferAPIExceptionHandler implements ExceptionMapper<MoneyTransferAPIException> {

    public MoneyTransferAPIExceptionHandler() {}

    public Response toResponse (MoneyTransferAPIException exception) {
        MoneyTransferAPIError apiError = new MoneyTransferAPIError(exception.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(apiError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
