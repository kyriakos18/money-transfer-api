package com.demo.moneytransferapi.utils;

import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2DatastoreFactory {
    protected static String driver = ConfigLoader.getPropertyAsString("h2.driver");
    protected static String connectionURL = ConfigLoader.getPropertyAsString("h2.connectionURL");
    protected static String userName = ConfigLoader.getPropertyAsString("h2.username");
    protected static String password = ConfigLoader.getPropertyAsString("h2.password");

    public H2DatastoreFactory(){
        DbUtils.loadDriver(driver);
    }

    public static void createTablesInH2() throws MoneyTransferAPIException{
        Connection h2Connection = null;
        try {
            h2Connection = getConnection();
            RunScript.execute(h2Connection, new FileReader("src/main/resources/H2CreateTables.sql"));
        } catch (SQLException ex){
            throw new MoneyTransferAPIException("");
        } catch (FileNotFoundException e){
            throw new MoneyTransferAPIException("Lala");
        } finally {
            DbUtils.closeQuietly(h2Connection);
        }
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(connectionURL, userName, password);
    }
}
