package com.demo.moneytransferapi.utils;

import com.demo.moneytransferapi.dao.AccountHolderImplementation;
import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MoneyTransferAPIHelper {

    private static final Logger LOGGER = Logger.getLogger(MoneyTransferAPIException.class);
    public static int prepareAccountBalanceUpdate (Connection connection, long accountID, BigDecimal balance) throws MoneyTransferAPIException {
        PreparedStatement statement = null;
        String updateAccountBalanceStatement = "UPDATE Account SET Balance = ? WHERE AccountId = ?";
        try{
            statement = connection.prepareStatement(updateAccountBalanceStatement);
            statement.setBigDecimal(1, balance);
            statement.setLong(2, accountID);
            LOGGER.debug("Will be executing statement " + updateAccountBalanceStatement + " for AccountID" + accountID +
                            " having Balance: " + balance.setScale(2, RoundingMode.HALF_EVEN));
            return statement.executeUpdate();
        } catch (SQLException ex){
            throw new MoneyTransferAPIException("Failed to lock account for update", ex);
        } finally {
            DbUtils.closeQuietly(statement);
        }

    }

    public static DefaultAccount checkIfAccountExistsLockAndGetState (Connection connection, long accountID) throws MoneyTransferAPIException{
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        String lockAccountStatement = "SELECT * FROM Account WHERE AccountID = ? FOR UPDATE";
        try {
            statement = connection.prepareStatement(lockAccountStatement);
            statement.setLong(1, accountID);
            LOGGER.debug("Executed Statement " + lockAccountStatement + " for AccountID: " + accountID);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                String accountHolderName = resultSet.getString("AccountHolderName");
                BigDecimal balance = resultSet.getBigDecimal("Balance");
                String currencyCode = resultSet.getString("CurrencyCode");
                DefaultAccount defaultAccount = new DefaultAccount(accountID, accountHolderName, balance, currencyCode);
                LOGGER.debug("Locked " + defaultAccount + " for update");
                return defaultAccount;
            }
            throw new MoneyTransferAPIException(String.format("Account with AccountID: %s does not exist", accountID));
        } catch (SQLException ex) {
            throw new MoneyTransferAPIException(String.format("Failed to lock account with AccountID: %s for " +
                    "update", accountID),ex);
        } finally {
            DbUtils.closeQuietly(statement);
            DbUtils.closeQuietly(resultSet);
        }
    }

    public static int updateAccountBalances(Connection connection, long accountFromID, long accountToID,
                                      BigDecimal balanceFrom, BigDecimal balanceTo) throws MoneyTransferAPIException{
        PreparedStatement statement = null;
        try {
            String updateAccountBalanceStatement = "UPDATE Account SET Balance = ? WHERE AccountId = ?";
            statement = connection.prepareStatement(updateAccountBalanceStatement);
            statement.setBigDecimal(1, balanceFrom);
            statement.setLong(2, accountFromID);
            statement.addBatch();
            statement.setBigDecimal(1, balanceTo);
            statement.setLong(2, accountToID);
            statement.addBatch();
            int[] updatedIDs = statement.executeBatch();
            connection.commit();
            return updatedIDs.length;
        } catch (SQLException ex){
            throw new MoneyTransferAPIException(String.format("Failed to update account balances for accounts with " +
                    "AccountIDs: %s, %s", accountFromID, accountToID), ex);
        } finally {
            DbUtils.closeQuietly(statement);
        }
    }
}
