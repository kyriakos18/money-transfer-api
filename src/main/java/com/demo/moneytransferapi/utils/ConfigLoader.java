package com.demo.moneytransferapi.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class ConfigLoader {

    private static final ThreadLocal<Properties> properties = new ThreadLocal<>();

    static {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appConfigPath = rootPath + "application.properties";
        properties.set(new Properties());
        loadConfig(appConfigPath);
    }

    private static void loadConfig(String configPath){
        try (InputStream input = new FileInputStream(configPath)){
            properties.get().load(input);
        } catch (IOException ex) {
            //log exception
        }
    }

    static String getPropertyAsString(String key){
        return properties.get().getProperty(key);
    }
}
