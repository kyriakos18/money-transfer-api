package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;

import java.math.BigDecimal;
import java.util.List;

public interface Account {
    /**
     * @param account The DefaultAccount Object having an AccountHolderName, a Balance, and Currency Code.
     * @return AccountID System generated AccountID for this account.
     * @throws MoneyTransferAPIException if account already exists, or SQLException.
     */
    long createAccount(DefaultAccount account) throws MoneyTransferAPIException;

    /**
     * @param accountID the System generated unique accountID for the account to be deleted.
     * @return numberOfRecords deleted. Min = 0 Max = 1.
     * @throws MoneyTransferAPIException if SQLException.
     */
    int deleteAccount(long accountID) throws MoneyTransferAPIException;

    /**
     * @param accountID System generated unique accountID, used to fetch the Account. Null if does not exist.
     * @return DefaultAccount The Account Object having an AccountHolderName, a Balance, CurrencyCode and AccountID.
     * @throws MoneyTransferAPIException if SQLException.
     */
    DefaultAccount getAccount(long accountID) throws MoneyTransferAPIException;

    /**
     * @param accountID System generated unique accountID, used to find the Account to deposit to.
     * @param amount the amount to be deposited in BigDecimal (2 dp). Must be greater than 0.
     * @return the number of records updated. Min = 0, Max = 1.
     * @throws MoneyTransferAPIException if amount <= 0 or account does not exist or SQLException.
     */
    int depositTo(long accountID, BigDecimal amount) throws MoneyTransferAPIException;

    /**
     * @param accountID System generated unique accountID, used to find the Account to withdraw from.
     * @param amount the amount to be withdrawn in BigDecimal (2 dp). Must be greater than 0.
     * @return the number of records updated. Min = 0, Max = 1.
     * @throws MoneyTransferAPIException if amount <= 0 or account does not exist or SQLException.
     */
    int withdrawFrom(long accountID, BigDecimal amount) throws MoneyTransferAPIException;

    /**
     * @return List of all the accounts in the system, as DefaultAccount objects.
     * @throws MoneyTransferAPIException if SQLException.
     */
    List<DefaultAccount> getAllAccounts() throws MoneyTransferAPIException;
}
