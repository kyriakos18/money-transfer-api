package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultTransfer;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;

public interface Transfer {
    /**
     * @param transfer A representation of a transfer having AccountFromID, AccountToID, Amount, Currency
     * @return
     * @throws MoneyTransferAPIException
     */
    int performTransfer(DefaultTransfer transfer) throws MoneyTransferAPIException;
}
