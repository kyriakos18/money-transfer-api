package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import com.demo.moneytransferapi.utils.H2DatastoreFactory;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.demo.moneytransferapi.utils.MoneyTransferAPIHelper.checkIfAccountExistsLockAndGetState;
import static com.demo.moneytransferapi.utils.MoneyTransferAPIHelper.prepareAccountBalanceUpdate;

public class AccountImplementation implements Account {
    private static final Logger LOGGER = Logger.getLogger(AccountImplementation.class);
    @Override
    public long createAccount(DefaultAccount account) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        String createAccountStatement = "INSERT INTO Account (AccountHolderName, Balance, CurrencyCode) VALUES (?, ?, ?)";
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            statement = h2Connection.prepareStatement(createAccountStatement);
            statement.setString(1, account.getAccountHolderName());
            statement.setBigDecimal(2, account.getBalance());
            statement.setString(3, account.getCurrencyCode());
            LOGGER.debug(String.format("Will be executing statement %s for %s",createAccountStatement, account));
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
            throw new MoneyTransferAPIException(String.format("%s, has not been created. No AccountID " +
                    "has been created", account));
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in creating %s", account), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
    }

    @Override
    public int deleteAccount(long accountID) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        String deleteAccountStatement = "DELETE FROM Account WHERE AccountID = ?";
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            statement = h2Connection.prepareStatement(deleteAccountStatement);
            statement.setLong(1, accountID);
            LOGGER.debug(String.format("Will be executing statement %s for AccountID: %s", deleteAccountStatement, accountID));
            return statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in deleting Account with ID: %s",
                    accountID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
    }


    @Override
    public DefaultAccount getAccount(long accountID) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        String getAccountStatement = "SELECT * FROM Account WHERE AccountID = ? ";
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            statement = h2Connection.prepareStatement(getAccountStatement);
            statement.setLong(1, accountID);
            LOGGER.debug(String.format("Will be executing statement %s for AccountID: %s", getAccountStatement, accountID));
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                String accountHolderName = resultSet.getString("AccountHolderName");
                BigDecimal balance = resultSet.getBigDecimal("Balance");
                String currencyCode = resultSet.getString("CurrencyCode");
                DefaultAccount defaultAccount = new DefaultAccount(accountID, accountHolderName, balance, currencyCode);
                LOGGER.debug(String.format("Found %s for AccountID: %s", defaultAccount, accountID));
                return defaultAccount;
            }
            LOGGER.debug(String.format("No account holder found for AccountID: %s", accountID));
            return null;
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in getting Account with AccountID: %s",
                    accountID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection, statement, resultSet);
        }
    }

    @Override
    public int depositTo(long accountID, BigDecimal amount) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            h2Connection.setAutoCommit(false);
            DefaultAccount accountLatestState = checkIfAccountExistsLockAndGetState(h2Connection, accountID);
            BigDecimal newAccountBalance = accountLatestState.getBalance().add(amount);
            int numberOfUpdatedRecords = prepareAccountBalanceUpdate(h2Connection, accountID, newAccountBalance);
            h2Connection.commit();
            LOGGER.debug(String.format("%s updated. %s deposited in account with AccountID: ", numberOfUpdatedRecords,
                    amount.setScale(2, RoundingMode.HALF_EVEN), accountID));
            return numberOfUpdatedRecords;
        } catch (SQLException ex){
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in depositing Amount: %s, in Account with " +
                            "AccountID: %s", amount.setScale(2, RoundingMode.HALF_EVEN), accountID), ex);
        } finally {
          DbUtils.closeQuietly(h2Connection);
        }
    }

    @Override
    public int withdrawFrom(long accountID, BigDecimal amount) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            h2Connection.setAutoCommit(false);
            DefaultAccount accountLatestState = checkIfAccountExistsLockAndGetState(h2Connection, accountID);
            BigDecimal newAccountBalance = accountLatestState.getBalance().subtract(amount);
            if (newAccountBalance.compareTo(BigDecimal.ZERO) < 0) {
                throw new MoneyTransferAPIException(String.format("Encountered error whilst withdrawing amount for Account with " +
                        "AccountID: %s. Cause: Insufficient Balance. Current Balance: %s", accountID,
                        accountLatestState.getBalance().setScale(2, RoundingMode.HALF_EVEN)));
            }
            int numberOfUpdatedRecords = prepareAccountBalanceUpdate(h2Connection, accountID, newAccountBalance);
            h2Connection.commit();
            LOGGER.debug(numberOfUpdatedRecords + " updated. " + amount.setScale(2, RoundingMode.HALF_EVEN)
                    + " withdrawn from account with AccountID: " + accountID);
            return numberOfUpdatedRecords;
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in withdrawing amount %s, from Account with " +
                    "AccountID: %s", amount.setScale(2, RoundingMode.HALF_EVEN), accountID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
        }
    }

    @Override
    public List<DefaultAccount> getAllAccounts() throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<DefaultAccount> accounts = new ArrayList<>();
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String getAllAccountsSQLStatement = "SELECT * FROM Account ";
            statement = h2Connection.prepareStatement(getAllAccountsSQLStatement);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                DefaultAccount account = new DefaultAccount(
                        resultSet.getLong("AccountID"),
                        resultSet.getString("AccountHolderName"),
                        resultSet.getBigDecimal("Balance"),
                        resultSet.getString("CurrencyCode")
                );
                accounts.add(account);
            }
            return accounts;
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException("Failed in getting all Accounts", ex);
        } finally {
            DbUtils.closeQuietly(h2Connection, statement, resultSet);
        }
    }
}
