package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultAccountHolder;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;

import java.util.List;

public interface AccountHolder {

    /**
     * @param accountHolder The DefaultAccount Object having an AccountHolderName, an EmailAddress, and PersonalID.
     * @return AccountHolderID. System generated AccountHolderID for this account holder.
     * @throws MoneyTransferAPIException if AccountHolder already exists, or SQLException.
     */
    long createAccountHolder(DefaultAccountHolder accountHolder) throws MoneyTransferAPIException;

    /**
     * @param accountHolderID the System generated unique accountHolderID for the account to be deleted.
     * @retur number Of records deleted. Min = 0 Max = 1.
     * @throws MoneyTransferAPIException if SQLException.
     */
    int deleteAccountHolder(long accountHolderID) throws MoneyTransferAPIException;

    /**
     * @param accountHolder The new state of the account holder represented as a DefaultAccountHolder object.
     * @return number of records updated. Min = 0, Max = 1.
     * @throws MoneyTransferAPIException if SQLException.
     */
    int updateAccountHolderInfo(DefaultAccountHolder accountHolder) throws MoneyTransferAPIException;

    /**
     * @param personalID The personalID for account holder. Unique to each individual.
     * @return number of records found. 1 if found, 0 if not.
     * @throws MoneyTransferAPIException if SQLException.
     */
    int checkIfAccountHolderExists(String personalID) throws MoneyTransferAPIException;

    /**
     * @param personalID The personalID for account holder. Unique to each individual.
     * @return DefaultAccountHolder. The account holder representation of an existing Account Holder.
     * @throws MoneyTransferAPIException if SQLException.
     */
    DefaultAccountHolder getAccountHolder(String personalID) throws MoneyTransferAPIException;

    /**
     * @return List of all the account holders in the system, as DefaultAccountHolder objects.
     * @throws MoneyTransferAPIException if SQLException.
     */
    List<DefaultAccountHolder> getAllAccountHolders() throws MoneyTransferAPIException;
}
