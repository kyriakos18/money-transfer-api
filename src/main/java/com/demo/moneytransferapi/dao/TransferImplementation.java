package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.DefaultTransfer;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import com.demo.moneytransferapi.utils.H2DatastoreFactory;
import com.demo.moneytransferapi.utils.MoneyTransferAPIHelper;
import org.apache.commons.dbutils.DbUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import static com.demo.moneytransferapi.utils.MoneyTransferAPIHelper.checkIfAccountExistsLockAndGetState;

public class TransferImplementation implements Transfer {
    @Override
    public int performTransfer(DefaultTransfer transaction) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            h2Connection.setAutoCommit(false);
            DefaultAccount accountFromLatestState = checkIfAccountExistsLockAndGetState(h2Connection, transaction.getAccountFromID());
            DefaultAccount accountToLatestState = checkIfAccountExistsLockAndGetState(h2Connection, transaction.getAccountToID());
            BigDecimal accountBalanceFromAfterTransfer = accountFromLatestState.getBalance()
                    .subtract(transaction.getAmount());
            if (accountBalanceFromAfterTransfer.compareTo(BigDecimal.ZERO) < 0) {
                throw new MoneyTransferAPIException(String.format("Encountered error whilst withdrawing " +
                                "amount from %s. Cause: Insufficient Balance", accountFromLatestState));
            }
            BigDecimal accountBalanceToAfterTransaction = accountToLatestState.getBalance().add(transaction.getAmount());
            return MoneyTransferAPIHelper.updateAccountBalances(h2Connection, accountFromLatestState.getAccountID(),
                    accountToLatestState.getAccountID(), accountBalanceFromAfterTransfer, accountBalanceToAfterTransaction);

        } catch (SQLException ex){
            try {
                h2Connection.rollback();
                return 0;
            } catch (SQLException | NullPointerException rollBackEx){
                throw new MoneyTransferAPIException("Failed to rollback transfer.", rollBackEx);
            }
        } finally {
            DbUtils.closeQuietly(h2Connection);
        }
    }
}
