package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.model.DefaultAccountHolder;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import com.demo.moneytransferapi.utils.H2DatastoreFactory;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountHolderImplementation implements AccountHolder {

    private static final Logger LOGGER = Logger.getLogger(AccountHolderImplementation.class);

    @Override
    public long createAccountHolder(DefaultAccountHolder accountHolder) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String createAccountHolderStatement = "INSERT INTO AccountHolder (AccountHolderName, EmailAddress, PersonalID) VALUES (?, ?, ?)";
            statement = h2Connection.prepareStatement(createAccountHolderStatement, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, accountHolder.getAccountHolderName());
            statement.setString(2, accountHolder.getEmailAddress());
            statement.setString(3, accountHolder.getPersonalID());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            LOGGER.debug(String.format("Executed Statement %s for %s", createAccountHolderStatement, accountHolder));
            if (resultSet.next()) {
                LOGGER.debug(String.format("Newly generated AccountHolderID: %s", resultSet.getLong(1)));
                return resultSet.getLong(1);
            }
            throw new MoneyTransferAPIException(String.format("%s, has not been created. No AccountHolderID " +
                    "has been created", accountHolder));
        } catch (SQLException ex){
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in creating %s", accountHolder), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
    }

    @Override
    public int deleteAccountHolder(long accountHolderID) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        String deleteAccountStatement = "DELETE FROM AccountHolder WHERE AccountHolderID = ?";
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            statement = h2Connection.prepareStatement(deleteAccountStatement);
            statement.setLong(1, accountHolderID);
            LOGGER.debug(String.format("Will be executing statement %s for AccountHolderID: %s", deleteAccountStatement,
                    accountHolderID));
            return statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in deleting Account Holder with ID: %s",
                    accountHolderID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
    }

    @Override
    public int updateAccountHolderInfo(DefaultAccountHolder accountHolder) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String updateAccountHolderInfoStatement = "UPDATE AccountHolder SET AccountHolderName = ?, EmailAddress = ?, PersonalID = ? WHERE AccountHolderID = ? ";
            statement = h2Connection.prepareStatement(updateAccountHolderInfoStatement);
            statement.setString(1, accountHolder.getAccountHolderName());
            statement.setString(2, accountHolder.getEmailAddress());
            statement.setString(3, accountHolder.getPersonalID());
            statement.setLong(4, accountHolder.getAccountHolderID());
            LOGGER.debug(String.format("Will be executing statement %s for AccountHolderID: %s",
                    updateAccountHolderInfoStatement, accountHolder));
            return statement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException(String.format("Failed in deleting %s", accountHolder), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
    }

    @Override
    public DefaultAccountHolder getAccountHolder(String personalID) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String getAccountHolderSQLStatement = "SELECT * FROM AccountHolder WHERE personalID = ?";
            statement = h2Connection.prepareStatement(getAccountHolderSQLStatement);
            statement.setString(1, personalID);
            LOGGER.debug(String.format("Will be executing statement %s for PersonalID: %s",
                    getAccountHolderSQLStatement, personalID));
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                long accountHolderID = resultSet.getLong("AccountHolderID");
                String accountHolderName = resultSet.getString("AccountHolderName");
                String emailAddress = resultSet.getString("EmailAddress");
                DefaultAccountHolder accountHolder = new DefaultAccountHolder(
                        accountHolderID, accountHolderName, emailAddress, personalID
                );
                LOGGER.debug(String.format("Found %s for PersonalID: %s", accountHolder, personalID));
                return accountHolder;
            }
            LOGGER.debug(String.format("No account holder found for PersonalID: %s", personalID));
            return null;
        } catch (SQLException ex) {
            LOGGER.debug("Encountered SQLException "+ ex.getMessage());
            throw new MoneyTransferAPIException(String.format("Failed in getting Account Holder with PersonalID: %s",
                    personalID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection, statement, resultSet);
        }
    }

    @Override
    public int checkIfAccountHolderExists(String personalID) throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int recordsFound = 0;
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String getAccountHolderSQLStatement = "SELECT count(*) FROM AccountHolder WHERE PersonalID = ? ";
            statement = h2Connection.prepareStatement(getAccountHolderSQLStatement);
            statement.setString(1,  personalID);
            LOGGER.debug(String.format("Will be executing statement %s, for PersonalID: %s",
                    getAccountHolderSQLStatement, personalID));
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                LOGGER.debug(String.format("Found Account Holder with AccountHolderID: %s", resultSet.getLong(1)));
                recordsFound = resultSet.getInt(1);
            }
            return recordsFound;
        } catch (SQLException ex) {
            LOGGER.debug("Encountered SQLException "+ ex.getMessage());
            throw new MoneyTransferAPIException(String.format("Failed whilst checking if Account Holder with " +
                    "PersonaID: %s exists.", personalID), ex);
        } finally {
            DbUtils.closeQuietly(h2Connection, statement, resultSet);
        }
    }

    @Override
    public List<DefaultAccountHolder> getAllAccountHolders() throws MoneyTransferAPIException {
        Connection h2Connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<DefaultAccountHolder> accountHolders = new ArrayList<>();
        try {
            h2Connection = H2DatastoreFactory.getConnection();
            String getAllAccountHoldersSQLStatement = "SELECT * FROM AccountHolder ";
            statement = h2Connection.prepareStatement(getAllAccountHoldersSQLStatement);
            LOGGER.debug(String.format("Will be executing statement %s" , getAllAccountHoldersSQLStatement));
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                DefaultAccountHolder accountHolder = new DefaultAccountHolder(
                        resultSet.getLong("AccountHolderID"),
                        resultSet.getString("AccountHolderName"),
                        resultSet.getString("EmailAddress"),
                        resultSet.getString("PersonalID")
                );
                accountHolders.add(accountHolder);
            }
            return accountHolders;
        } catch (SQLException ex) {
            LOGGER.debug(String.format("Encountered SQLException %s", ex.getMessage()));
            throw new MoneyTransferAPIException("Failed in getting all Account Holders", ex);
        } finally {
            DbUtils.closeQuietly(h2Connection, statement, resultSet);
        }
    }
}
