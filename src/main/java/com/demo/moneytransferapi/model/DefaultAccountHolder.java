package com.demo.moneytransferapi.model;

public class DefaultAccountHolder {
    private long accountHolderID;
    private String accountHolderName;
    private String emailAddress;
    private String personalID;

    public DefaultAccountHolder() {}

    public DefaultAccountHolder(String accountHolderName, String emailAddress, String personalID){
        this.accountHolderName = accountHolderName;
        this.emailAddress = emailAddress;
        this.personalID = personalID;
    }

    public DefaultAccountHolder(long accountHolderID, String accountHolderName, String emailAddress, String personalID){
        this.accountHolderID = accountHolderID;
        this.accountHolderName = accountHolderName;
        this.emailAddress = emailAddress;
        this.personalID = personalID;
    }

    public long getAccountHolderID() {
        return accountHolderID;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPersonalID(){
        return personalID;
    }

    public void setAccountHolderID(long accountHolderID) {
        this.accountHolderID = accountHolderID;
    }

    @Override
    public String toString() {
        return "AccountHolder: [ " + accountHolderName + ", " + emailAddress + ", " + personalID  + " ]";
    }
}
