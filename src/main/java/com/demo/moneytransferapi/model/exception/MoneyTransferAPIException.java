package com.demo.moneytransferapi.model.exception;

public class MoneyTransferAPIException extends Exception{
    public MoneyTransferAPIException(String errorMessage) {
        super(errorMessage);
    }
    public MoneyTransferAPIException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
