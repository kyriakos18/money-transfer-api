package com.demo.moneytransferapi.model.exception;

public class MoneyTransferAPIError {

    private String responseCode;

    public MoneyTransferAPIError(String responseCode){
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
