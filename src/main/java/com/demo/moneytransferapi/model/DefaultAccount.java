package com.demo.moneytransferapi.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DefaultAccount {

    private long accountID;
    private BigDecimal balance;
    private String accountHolderName;
    private String currencyCode;

    public DefaultAccount (String accountHolderName, BigDecimal balance, String currencyCode){
        this.accountHolderName = accountHolderName;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public DefaultAccount (long accountID, String accountHolderName, BigDecimal balance, String currencyCode){
        this.accountID = accountID;
        this.accountHolderName = accountHolderName;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public long getAccountID() {
        return accountID;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Account: [ " + accountID + ", " + accountHolderName + ", "
                + currencyCode + ", " + balance.setScale(2, RoundingMode.HALF_EVEN) + " ]";
    }
}
