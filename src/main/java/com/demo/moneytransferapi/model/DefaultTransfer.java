package com.demo.moneytransferapi.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DefaultTransfer {
    private long accountFromID;
    private long accountToID;
    private BigDecimal amount;
    private String currencyCode;

    public DefaultTransfer(long accountFromID, long accountToID, BigDecimal amount, String currencyCode){
        this.accountFromID = accountFromID;
        this.accountToID = accountToID;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }

    public long getAccountFromID() {
        return accountFromID;
    }

    public long getAccountToID() {
        return accountToID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transfer [ " + accountFromID + ", " + accountToID + ", "
                + currencyCode + ", " + amount.setScale(2, RoundingMode.HALF_EVEN) + " ]";
    }
}
