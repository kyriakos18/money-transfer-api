INSERT INTO AccountHolder (AccountHolderName, EmailAddress, PersonalID) VALUES ('KyriakosPieris','kyriakos.pieris@gmail.com', 'KP100');
INSERT INTO AccountHolder (AccountHolderName, EmailAddress, PersonalID) VALUES ('MariaPieri','maria.pieri@gmail.com', 'MP200');
INSERT INTO AccountHolder (AccountHolderName, EmailAddress, PersonalID) VALUES ('DemetrisPieris','demetris.pieria@gmail.com', 'DP300');
INSERT INTO AccountHolder (AccountHolderName, EmailAddress, PersonalID) VALUES ('ChrysoPieri','chryso.pieri@gmail.com', 'CP400');

INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('KyriakosPieris',100.00,'USD');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('MariaPieri',200.00,'USD');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('KyriakosPieris',300.00,'EUR');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('MariaPieri',400.00,'EUR');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('KyriakosPieris',500.00,'GBP');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('DemetrisPieris',10.00,'AUD');
INSERT INTO Account (AccountHolderName,Balance,CurrencyCode) VALUES ('ChrysoPieri',10.00,'GBP');