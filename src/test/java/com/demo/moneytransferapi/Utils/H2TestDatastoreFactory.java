package com.demo.moneytransferapi.Utils;

import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import com.demo.moneytransferapi.utils.H2DatastoreFactory;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

public class H2TestDatastoreFactory extends H2DatastoreFactory {
    static {
        driver = "org.h2.Driver";
        connectionURL = "jdbc:h2:mem:money-transfer-api;DB_CLOSE_DELAY=-1";
        userName = "username";
        password = "password";
    }

    public H2TestDatastoreFactory(){
        super();
    }

    public static void populateWithMockData() throws MoneyTransferAPIException{
        Connection h2Connection = null;
        try {
            h2Connection = getConnection();
            RunScript.execute(h2Connection, new FileReader("src/test/resources/H2InsertMockData.sql"));
        } catch (SQLException ex){
            throw new MoneyTransferAPIException("");
        } catch (FileNotFoundException e){
            throw new MoneyTransferAPIException("Lala");
        } finally {
            DbUtils.closeQuietly(h2Connection);
        }
    }

    public static void truncateTables() throws MoneyTransferAPIException {
        Connection h2Connection = null;
        try {
            h2Connection = getConnection();
            RunScript.execute(h2Connection, new FileReader("src/test/resources/H2TruncateTables.sql"));
        } catch (SQLException ex){
            throw new MoneyTransferAPIException("");
        } catch (FileNotFoundException e){
            throw new MoneyTransferAPIException("Lala");
        } finally {
            DbUtils.closeQuietly(h2Connection);
        }
    }
}
