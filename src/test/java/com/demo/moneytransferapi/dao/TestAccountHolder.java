package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.demo.moneytransferapi.model.DefaultAccountHolder;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;

public class TestAccountHolder {

    private static H2TestDatastoreFactory h2DatastoreFactory;
    private static AccountHolderImplementation accountHolderImpl;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void setup() throws MoneyTransferAPIException {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        accountHolderImpl = new AccountHolderImplementation();
    }

    @Test
    public void createAccountHolderShouldCreateAccountHolderIfDoesNotExist() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = new DefaultAccountHolder("MatthewReilly",
                "matthew.reilly@gmail.com", "MR500");
        accountHolderImpl.createAccountHolder(accountHolder);
        DefaultAccountHolder accountHolderFound = accountHolderImpl.getAccountHolder("MR500");
        assertEquals(accountHolder.getAccountHolderName(), accountHolderFound.getAccountHolderName());
    }

    @Test
    public void createAccountHolderShouldThrowExceptionWhenAccountHolderAlreadyExists() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = new DefaultAccountHolder("KyriakosPieris",
                "kyriakos.pieris@gmail.com", "KP100");
        exception.expect(MoneyTransferAPIException.class);
        exception.expectMessage("Failed in creating AccountHolder: [ KyriakosPieris, kyriakos.pieris@gmail.com, KP100 ]");
        accountHolderImpl.createAccountHolder(accountHolder);
    }

    @Test
    public void getAccountHolderShouldReturnCorrectAccountHolderIfPresent() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = accountHolderImpl.getAccountHolder("MP200");
        Assert.assertEquals(accountHolder.getAccountHolderName(), "MariaPieri");
    }

    @Test
    public void getAccountHolderShouldReturnNullIfAccountHolderNotPresent() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = accountHolderImpl.getAccountHolder("KP200");
        assertNull(accountHolder);
    }

    @Test
    public void updateAccountHolderShouldChangeAccountHolderInformationWhenPresent() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = new DefaultAccountHolder(1L, "KyriakosPieris",
                "kyriakos_pieris@outlook.com", "KP100");
        int updatedRows = accountHolderImpl.updateAccountHolderInfo(accountHolder);
        assertEquals(1, updatedRows);
        DefaultAccountHolder updatedAccountHolder = accountHolderImpl.getAccountHolder(accountHolder.getPersonalID());
        assertEquals(accountHolder.getEmailAddress(), updatedAccountHolder.getEmailAddress());
    }

    @Test
    public void updateAccountHolderShouldNotUpdateAnyRecordsIfAccountHolderNotPresent() throws MoneyTransferAPIException {
        DefaultAccountHolder accountHolder = new DefaultAccountHolder(10L, "KyriakosPieris",
                "kyriakos_pieris@outlook.com", "KP500");
        int updatedRows = accountHolderImpl.updateAccountHolderInfo(accountHolder);
        assertEquals(0, updatedRows);
    }

    @Test
    public void deleteAccountHolderShouldRemoveHolderWhenPresent() throws MoneyTransferAPIException {
        int deletedRows = accountHolderImpl.deleteAccountHolder(3L);
        assertEquals(1, deletedRows);
        assertNull(accountHolderImpl.getAccountHolder("DP300"));
    }

    @Test
    public void deleteAccountHolderShouldNotDeleteAnyRecordsIfAccountHolderNotPresent() throws MoneyTransferAPIException {
        int deletedRows = accountHolderImpl.deleteAccountHolder(11L);
        assertEquals(0, deletedRows);
    }

    @Test
    public void getAllAccountHoldersShouldReturnAListOfAllTheAccountHolders() throws MoneyTransferAPIException{
        List<DefaultAccountHolder> defaultAccountHolders = accountHolderImpl.getAllAccountHolders();
        assertTrue(!defaultAccountHolders.isEmpty());
    }
}
