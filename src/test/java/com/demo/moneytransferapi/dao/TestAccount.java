package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static org.junit.Assert.*;

public class TestAccount {

    private static H2TestDatastoreFactory h2DatastoreFactory;
    private static AccountImplementation accountImpl;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void setup() throws MoneyTransferAPIException {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        accountImpl = new AccountImplementation();
    }

    @Test
    public void getAllAccountsShouldReturnAListOfAllTheAccounts() throws MoneyTransferAPIException {
        List<DefaultAccount> accounts = accountImpl.getAllAccounts();
        assertTrue(!accounts.isEmpty());
    }

    @Test
    public void getAccountShouldReturnCorrectAccountIfPresent() throws MoneyTransferAPIException {
        DefaultAccount account = accountImpl.getAccount(1L);
        assertEquals("KyriakosPieris", account.getAccountHolderName());
    }

    @Test
    public void getAccountShouldReturnNullWhenAccountIfNotPresent() throws MoneyTransferAPIException {
        DefaultAccount account = accountImpl.getAccount(15L);
        assertNull(account);
    }

    @Test
    public void createAccountShouldCreateANewAccountIfNotPresent() throws MoneyTransferAPIException {
        BigDecimal balance = new BigDecimal(10.00).setScale(2, RoundingMode.HALF_EVEN);
        DefaultAccount newAccount = new DefaultAccount("MatthewReilly", balance, "AUD");
        long newAccountID = accountImpl.createAccount(newAccount);
        DefaultAccount newlyCreatedAccount = accountImpl.getAccount(newAccountID);
        assertEquals(newAccount.getAccountHolderName(), newlyCreatedAccount.getAccountHolderName());
        assertEquals(newAccount.getCurrencyCode(), newlyCreatedAccount.getCurrencyCode());
        assertEquals(newAccount.getBalance(), newlyCreatedAccount.getBalance());
    }

    @Test
    public void deleteAccountShouldDeleteAccountIfPresent() throws MoneyTransferAPIException {
        int deletedRows = accountImpl.deleteAccount(3L);
        assertEquals(1, deletedRows);
        assertNull(accountImpl.getAccount(3l));
    }

    @Test
    public void deleteAccountShouldNotDeleteAccountIfNotPresent() throws MoneyTransferAPIException {
        int deletedRows = accountImpl.deleteAccount(15L);
        assertEquals(0, deletedRows);
    }

    @Test
    public void depositToShouldUpdateAccountBalanceWhenAccountIsPresent() throws MoneyTransferAPIException {
        BigDecimal depositAmount = new BigDecimal(200).setScale(4, RoundingMode.HALF_EVEN);
        int updatedRows = accountImpl.depositTo(1L, depositAmount);
        assertEquals(1, updatedRows);
        BigDecimal updatedBalance = new BigDecimal(300).setScale(2, RoundingMode.HALF_EVEN);
        DefaultAccount updatedAccount = accountImpl.getAccount(1L);
        assertEquals(updatedBalance, updatedAccount.getBalance());
    }

    @Test
    public void depositToShouldThrowAndExceptionWhenAccountIsNotPresent() throws MoneyTransferAPIException {
        BigDecimal depositAmount = new BigDecimal(200).setScale(4, RoundingMode.HALF_EVEN);
        exception.expect(MoneyTransferAPIException.class);
        exception.expectMessage("Account with AccountID: 15 does not exist");
        accountImpl.depositTo(15L, depositAmount);
    }

    @Test
    public void withdrawFromShouldUpdateAccountBalanceIfAccountIsPresentAndHasSufficientBalance() throws MoneyTransferAPIException{
        BigDecimal withdrawalAmount = new BigDecimal(10).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal updatedBalance = new BigDecimal(190).setScale(2, RoundingMode.HALF_EVEN);
        int updatedRows = accountImpl.withdrawFrom(2L, withdrawalAmount);
        assertEquals(1, updatedRows);
        assertEquals(updatedBalance, accountImpl.getAccount(2L).getBalance());
    }

    @Test
    public void withdrawFromShouldThrowAndExceptionWhenAccountIsNotPresent() throws MoneyTransferAPIException {
        BigDecimal withdrawalAmount = new BigDecimal(200).setScale(4, RoundingMode.HALF_EVEN);
        exception.expect(MoneyTransferAPIException.class);
        exception.expectMessage("Account with AccountID: 15 does not exist");
        accountImpl.withdrawFrom(15L, withdrawalAmount);
    }

    @Test
    public void withdrawFromShouldThrowAndExceptionWhenInsufficientBalance() throws MoneyTransferAPIException {
        BigDecimal withdrawalAmount = new BigDecimal(1000).setScale(4, RoundingMode.HALF_EVEN);
        exception.expect(MoneyTransferAPIException.class);
        exception.expectMessage("Insufficient Balance");
        accountImpl.withdrawFrom(4L, withdrawalAmount);
    }
}
