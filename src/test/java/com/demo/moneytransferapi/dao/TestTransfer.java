package com.demo.moneytransferapi.dao;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.demo.moneytransferapi.model.DefaultAccount;
import com.demo.moneytransferapi.model.DefaultTransfer;
import com.demo.moneytransferapi.model.exception.MoneyTransferAPIException;
import org.apache.commons.dbutils.DbUtils;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.concurrent.CountDownLatch;

public class TestTransfer {

    private static H2TestDatastoreFactory h2DatastoreFactory;
    private static TransferImplementation transferImpl;
    private static AccountImplementation accountImpl;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void setup() throws MoneyTransferAPIException {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        transferImpl = new TransferImplementation();
        accountImpl = new AccountImplementation();
    }

    @Test
    public void performTransferShouldTransferAmountBetweenAccountsWhenNotLockedAndSufficientFunds() throws MoneyTransferAPIException {
        DefaultAccount accountFromBeforeTransfer = accountImpl.getAccount(1L);
        DefaultAccount accountToBeforeTransfer = accountImpl.getAccount(2L);
        BigDecimal transferAmount = new BigDecimal(5.00).setScale(2, RoundingMode.HALF_EVEN);
        DefaultTransfer transfer = new DefaultTransfer(1L, 2L, transferAmount, "USD");
        transferImpl.performTransfer(transfer);
        DefaultAccount accountFromAfterTransfer = accountImpl.getAccount(1L);
        DefaultAccount accountToAfterTransfer = accountImpl.getAccount(2L);
        assertEquals(accountFromBeforeTransfer.getBalance().subtract(transferAmount), accountFromAfterTransfer.getBalance());
        assertEquals(accountToBeforeTransfer.getBalance().add(transferAmount), accountToAfterTransfer.getBalance());
    }

    @Test
    public void performTransferShouldThrowExceptionWhenInsufficientFundsInFromAccount() throws MoneyTransferAPIException {
        BigDecimal transferAmount = new BigDecimal(50000.00).setScale(2, RoundingMode.HALF_EVEN);
        DefaultTransfer transfer = new DefaultTransfer(1L, 2L, transferAmount, "USD");
        exception.expect(MoneyTransferAPIException.class);
        exception.expectMessage("Encountered error whilst withdrawing amount from Account: [ 1, KyriakosPieris, USD, 95.00 ]. " +
                        "Cause: Insufficient Balance");
        transferImpl.performTransfer(transfer);
    }

    @Test
    public void multipleTransfersForTheSameAccountShouldBeAppliedOneAfterTheOther() throws MoneyTransferAPIException, InterruptedException {
        final CountDownLatch countDownLatch = new CountDownLatch(10);
        BigDecimal transferAmount = new BigDecimal(10.00).setScale(2, RoundingMode.HALF_EVEN);
        DefaultTransfer transfer = new DefaultTransfer(1L, 2L, transferAmount, "USD");
        for (int thread = 0; thread <= 10; thread++) {
            new Thread(() -> {
                try {
                    transferImpl.performTransfer(transfer);
                } catch (MoneyTransferAPIException e) {

                } finally {
                    countDownLatch.countDown();
                }
            }).start();
        }

        countDownLatch.await();
        DefaultAccount accountFrom = accountImpl.getAccount(1L);
        DefaultAccount accountTo = accountImpl.getAccount(2L);
        // 10 rounds of 10, 1 round unsuccessful
        assertEquals(accountFrom.getBalance(), new BigDecimal(5).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(accountTo.getBalance(), new BigDecimal(295).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void performTransferShouldNotSucceedWhenAccountIsLocked() throws MoneyTransferAPIException {
        DefaultAccount accountFromBeforeTransfer = accountImpl.getAccount(1L);
        DefaultAccount accountToBeforeTransfer = accountImpl.getAccount(2L);
        String SQL_LOCK_ACC = "SELECT * FROM Account WHERE AccountId = 1 FOR UPDATE";
        Connection h2Connection = null;
        PreparedStatement statement = null;
        try {
            h2Connection = H2TestDatastoreFactory.getConnection();
            h2Connection.setAutoCommit(false);
            statement = h2Connection.prepareStatement(SQL_LOCK_ACC);
            statement.executeQuery();
            BigDecimal transferAmount = new BigDecimal(1).setScale(2, RoundingMode.HALF_EVEN);
            DefaultTransfer transfer = new DefaultTransfer(1L, 2L, transferAmount, "GBP");
            transferImpl.performTransfer(transfer);
            h2Connection.commit();
        } catch (Exception e) {
        } finally {
            DbUtils.closeQuietly(h2Connection);
            DbUtils.closeQuietly(statement);
        }
        DefaultAccount accountFromAfterTransfer = accountImpl.getAccount(1L);
        DefaultAccount accountToAfterTransfer = accountImpl.getAccount(2L);
        assertEquals(accountFromBeforeTransfer.getBalance(), accountFromAfterTransfer.getBalance());
        assertEquals(accountToBeforeTransfer.getBalance(), accountToAfterTransfer.getBalance());
    }

}
