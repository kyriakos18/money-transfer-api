package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.demo.moneytransferapi.model.DefaultAccountHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAccountHolderController {
    private static H2TestDatastoreFactory h2DatastoreFactory;
    private final String BASE_URI = "http://localhost:7070/accountHolder/";
    private static CloseableHttpClient client;
    private static Server server = null;


    @BeforeClass
    public static void setup() throws Exception {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        startServer();
        client = HttpClientBuilder.create().build();
    }

    @AfterClass
    public static void clean() throws Exception {
        HttpClientUtils.closeQuietly(client);
        server.stop();
    }

    private static void startServer() throws Exception {
        server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        server.setHandler(handler);
        ServletHolder servletHolder = handler.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                AccountHolderController.class.getCanonicalName() + "," +
                        AccountController.class.getCanonicalName());
        server.start();
    }

    @Test
    public void getAccountHolderShouldReturnAccountHolderIfExists() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URI + "{personalID}")
                .header("Content-type", "application/json")
                .routeParam("personalID", "CP400")
                .asJson();
        JSONObject accountHolderFound = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals("ChrysoPieri", accountHolderFound.get("accountHolderName"));

    }

    @Test
    public void getAllAccountHoldersShouldReturnAllExistingAccountHolders() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URI + "accountHolders").asJson();
        JSONArray accountHolders = response.getBody().getArray();
        assertTrue(accountHolders.length() > 0);
    }

    @Test
    public void createAccountHolderShouldCreateAnAccountHolderAsSpecified() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URI + "createAccountHolder/{accountHolderName}/{emailAddress}/{personalID}")
                .header("Content-type", "application/json")
                .routeParam("accountHolderName", "MaxKeller")
                .routeParam("emailAddress", "max.keller@gmail.com")
                .routeParam("personalID", "MK1000").asJson();
        JSONObject accountHolderCreated = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals("MaxKeller", accountHolderCreated.get("accountHolderName"));
        assertEquals("max.keller@gmail.com", accountHolderCreated.get("emailAddress"));
        assertEquals("MK1000", accountHolderCreated.get("personalID"));
    }

    @Test
    public void updateAccountHolderShouldChangeTheDetailsOfAnExistingAccountHolder() throws UnirestException {
        Unirest.setHttpClient(client);
        Unirest.setObjectMapper(new ObjectMapper() {
            com.fasterxml.jackson.databind.ObjectMapper mapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public String writeValue(Object value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException("Object Mapper");
                }
            }

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return mapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException("Object Mapper");
                }
            }
        });
        DefaultAccountHolder accountHolder = new DefaultAccountHolder(2L, "MariaPieri",
                "maria@gmail.com", "MP200");
        HttpResponse<JsonNode> response = Unirest.put(BASE_URI + "5")
                .header("Content-type", "application/json")
                .body(accountHolder)
                .asJson();
        assertEquals(200, response.getStatus());
    }


    @Test
    public void deleteAccountHolderShouldDeleteExistingAccountHolder() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.delete(BASE_URI + "1").asJson();
        assertEquals(200, response.getStatus());
    }
}
