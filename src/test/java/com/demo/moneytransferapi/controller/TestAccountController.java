package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


import static org.junit.Assert.*;


public class TestAccountController {

    private static H2TestDatastoreFactory h2DatastoreFactory;
    private final String BASE_URI = "http://localhost:7070/account/";
    private static CloseableHttpClient client;
    private static Server server = null;


    @BeforeClass
    public static void setup() throws Exception {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        startServer();
        client = HttpClientBuilder.create().build();
    }

    @AfterClass
    public static void clean() throws Exception {
        HttpClientUtils.closeQuietly(client);
        server.stop();
    }

    private static void startServer() throws Exception {
        server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        server.setHandler(handler);
        ServletHolder servletHolder = handler.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames", AccountController.class.getCanonicalName());
        server.start();
    }


    @Test
    public void createAccountShouldCreateANewAccountWhenItDoesNotExist() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.put(BASE_URI + "createAccount/{accountHolderName}/{currencyCode}/{balance}")
                .header("Content-type", "application/json")
                .routeParam("accountHolderName", "MaxKeller")
                .routeParam("currencyCode", "EUR")
                .routeParam("balance", "500").asJson();
        JSONObject accountCreated = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals("MaxKeller", accountCreated.get("accountHolderName"));
        assertEquals("EUR", accountCreated.get("currencyCode"));
        assertEquals(500.0, accountCreated.get("balance"));
    }

    @Test
    public void getAllAccountsShouldReturnAllAccountsInSystem() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URI + "accounts").asJson();
        JSONArray accounts = response.getBody().getArray();
        assertTrue(accounts.length() > 0);
    }

    @Test
    public void deleteAccountShouldSuccessfullyDeleteAccountIfItExists() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.delete(BASE_URI + "1").asJson();
        assertEquals(200, response.getStatus());
    }

    @Test
    public void depositToAccountShouldAddAmountToBalance() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.put(BASE_URI + "deposit/{accountID}/{amount}")
                .header("Content-type", "application/json")
                .routeParam("accountID", "1")
                .routeParam("amount", "50").asJson();
        JSONObject updatedAccount = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals(150.0, updatedAccount.get("balance"));
    }

    @Test
    public void withdrawFromAccountShouldRemoveAmountFromBalance() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.put(BASE_URI + "withdraw/{accountID}/{amount}")
                .header("Content-type", "application/json")
                .routeParam("accountID", "5")
                .routeParam("amount", "50").asJson();
        JSONObject updatedAccount = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals(450.0, updatedAccount.get("balance"));
    }

    @Test
    public void getAccountShouldReturnAccountIfItExists() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.get(BASE_URI + "1").asJson();
        JSONObject accountFound = response.getBody().getObject();
        assertEquals(200, response.getStatus());
        assertEquals("KyriakosPieris", accountFound.get("accountHolderName"));
    }
}
