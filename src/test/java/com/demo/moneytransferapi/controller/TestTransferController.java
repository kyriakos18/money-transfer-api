package com.demo.moneytransferapi.controller;

import com.demo.moneytransferapi.Utils.H2TestDatastoreFactory;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestTransferController {

    private static H2TestDatastoreFactory h2DatastoreFactory;
    private final String BASE_URI = "http://localhost:7070/transfer/";
    private static CloseableHttpClient client;
    private static Server server = null;


    @BeforeClass
    public static void setup() throws Exception {
        h2DatastoreFactory = new H2TestDatastoreFactory();
        H2TestDatastoreFactory.createTablesInH2();
        H2TestDatastoreFactory.populateWithMockData();
        startServer();
        client = HttpClientBuilder.create().build();
    }

    @AfterClass
    public static void clean() throws Exception {
        HttpClientUtils.closeQuietly(client);
        server.stop();
    }

    private static void startServer() throws Exception {
        server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        server.setHandler(handler);
        ServletHolder servletHolder = handler.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                TransferController.class.getCanonicalName());
        server.start();
    }

    @Test
    public void testTransactionEnoughFund() throws UnirestException {
        Unirest.setHttpClient(client);
        HttpResponse<JsonNode> response = Unirest.post(BASE_URI + "{accountFromID}/{accountToID}/{amount}")
                .header("Content-type", "application/json")
                .routeParam("accountFromID", "5")
                .routeParam("accountToID", "7")
                .routeParam("amount", "50").asJson();
        assertEquals(200, response.getStatus());
    }

}
